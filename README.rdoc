== README

This system is designed with cover-letter-writing in mind. It's boring. You have
to be innovative and personalised. Who can be bothered?

We take the advice from reed.co.uk (http://www.reed.co.uk/career-advice/blog/2012/september/how-to-write-a-cover-letter
- reproduced here) and help you build your cv from snippets.

==== What to include in your cover letter
===== Opening the letter
The opening paragraph should be short and to the point and explain why it is that you’re writing.

Example:
'I would like to be considered for the position of ‘IT Manager’.

It is also useful to include where you found the ad i.e. as advertised on
reed.co.uk or, if someone referred you to the contact, mention their name in
this section.

===== Second paragraph
Why are you suitable for the job? Briefly describe your professional and
academic qualifications that are relevant to the role and ensure you refer to
each of the skills listed in the job description.

===== Third paragraph
Here’s your opportunity to emphasise what you can do for the company. Outline
your career goal (make it relevant to the position you’re applying for) and
expand on pertinent points in your CV.

===== Fourth paragraph
Here’s where you reiterate your interest in the role and why you would be the
right fit for the role. It’s also a good time to indicate you’d like to meet
with the employer for an interview.

===== Closing the letter
Sign off your cover letter with ‘Yours sincerely’ and your name.
