class @CoverLettersController
  constructor: ($scope)->
    # Index coverletters
    $scope.page_title = "Coverletters ANGULAR home!"


    # New coverletter
    $scope.skills = {}

    $scope.update_letter = ->
      needed_skills = (k for k,v of @skills).filter (k)=> @skills[k]
      if needed_skills.length is 0
        @skills_text = 'I haz no skillz :('
      else
        @skills_text = "I have super-awesome abilities in "
        @skills_text += needed_skills.join(', ')
