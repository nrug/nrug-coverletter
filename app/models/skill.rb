class Skill < ActiveRecord::Base
  belongs_to :user

  scope :experiences, -> { where previous_experience: true }
  scope :non_experiences, -> { where previous_experience: false }
end
