class CoverLetter < ActiveRecord::Base
  belongs_to :user

  def addressee_or_generic
    return addressee unless addressee.blank?
    "Sir or Madam"
  end

  def filled_paragraphs
    [first_paragraph, second_paragraph, third_paragraph, fourth_paragraph].reject &:blank?
  end

  def signature
    return "faithfully" if addressee.blank?
    "sincerely"
  end
end
