class CoverLettersController < ApplicationController
  before_filter :authenticate_user!

  def show
    @cover_letter = CoverLetter.find params[:id]
  end

  def create
    CoverLetter.create! permitted_data.merge(:user => current_user)
    redirect_to root_url
  end


  private
  def permitted_data
    params[:cover_letter].permit :addressee, :first_paragraph,
          :second_paragraph, :third_paragraph, :fourth_paragraph,
          :your_name
  end
end
