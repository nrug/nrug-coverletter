class SkillsController < ApplicationController
  def show
    @skill = Skill.find params[:id]
  end

  def create
    Skill.create! permitted_data.merge(:user => current_user)
    redirect_to root_url
  end


  private
  def permitted_data
    params[:skill].permit :name, :previous_experience
  end
end
