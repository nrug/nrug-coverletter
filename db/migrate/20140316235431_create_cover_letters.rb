class CreateCoverLetters < ActiveRecord::Migration
  def change
    create_table :cover_letters do |t|
      t.string :addressee
      t.text :first_paragraph
      t.text :second_paragraph
      t.text :third_paragraph
      t.text :fourth_paragraph
      t.string :your_name

      t.timestamps
    end
  end
end
