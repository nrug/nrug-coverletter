class AddOwnerToCoverLetter < ActiveRecord::Migration
  def up
    add_column :cover_letters, :user_id, :integer
  end

  def down
    remove_column :cover_letters, :user_id
  end
end
